console.log('Hello World');
// Mini Activity

function getMessage(message){
	console.log("This is a message")
}

// getMessage();
// getMessage();
// getMessage();
// getMessage();
// getMessage();

// While loop
// a while loop allows us to repeat a task/code while the condition is true. 

/*
	Syntax:
		while(condition){
			task;
			increment/decrement;
		}
*/

let count = 5;

while(count!== 0){
	getMessage();
	count--;
}

count = 5;

while(count!== 0){
	console.log(count);
	count--;
}

// Do While

count = 5;
do{
	console.log(count);
	count--;
} while(count === 0);

// Mini Activity

let counterMini = 20;

while(counterMini!==0){
	console.log(counterMini)
	counterMini--;
}

// For Loop
for(count=0; count<=20; count++){
	console.log(count);
}

for (let x=0; x<10; x++){
	let sum = 1 + x;
	console.log('The sum of ' + 1 + ' + ' + x + ' = ' + sum);
}

// continue and break
for (let counter = 0; counter <= 20; counter++){
	if(counter%2 === 0){
		continue;
	}

	console.log(counter)
	if(counter === 1){
		break;
	}
}

for (five=5; five<=100; five+=5){
		console.log(five)
}


